//
//  LPAppDelegate.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/2/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
