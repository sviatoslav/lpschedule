//
//  LPMenuCellItem.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LPMenuCellItem <NSObject>

@property (nonatomic) NSString* valueText;
@property (nonatomic) NSString* descriptionText;

@end

typedef NSMutableDictionary<LPMenuCellItem> LPMenuCellItem;
