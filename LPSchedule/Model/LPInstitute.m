//
//  LPInstitute.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPInstitute.h"
#import "LPGroup.h"


@implementation LPInstitute

@dynamic code;
@dynamic name;
@dynamic groups;

@end
