//
//  LPGroup.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LPInstitute, LPLesson;

@interface LPGroup : NSManagedObject

@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) LPInstitute *institute;
@property (nonatomic, retain) NSSet *lessons;
@end

@interface LPGroup (CoreDataGeneratedAccessors)

- (void)addLessonsObject:(LPLesson *)value;
- (void)removeLessonsObject:(LPLesson *)value;
- (void)addLessons:(NSSet *)values;
- (void)removeLessons:(NSSet *)values;

@end
