//
//  LPDataProvider.m
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 5/2/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPDataProvider.h"
#import "JSONKit.h"

static dispatch_queue_t queue;

@implementation LPDataProvider

+(void)initialize
{
    queue = dispatch_queue_create("com.sviatoslav.yakymiv.data.provider", DISPATCH_QUEUE_SERIAL);
}

+ (void) getInstitutesWithCallback: (LPDataProviderCallback) callback
{
    [self LP_getDataFromURLString:@"http://schedule-lp.pp.ua/api/institute" withCallback:^(NSURLResponse *response, id JSONObject, NSError *error) {
        NSArray * institutes = JSONObject;
        NSMutableArray *result = [NSMutableArray array];
        for(NSMutableDictionary *institute in institutes)
        {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            for(NSString *key in institute.allKeys)
            {
                dict[key] = institute[key];
            }
            dict[@"code"] = dict[@"iid"];
            [result addObject:dict];
        }
        callback(response, result, error);
    }];
}

+ (void) getGroupsForInstituteID: (int) instituteID withCallback: (LPDataProviderCallback) callback
{
    [self LP_getDataFromURLString:[NSString stringWithFormat:@"http://schedule-lp.pp.ua/api/group?inst=%d",instituteID] withCallback:callback];
}

+ (void) getScheduleForInstituteID:(int)instituteID groupID:(int)groupID callback:(LPDataProviderCallback)callback
{
    [self getScheduleForInstituteID:instituteID groupID:groupID semestr:LPSemestrSpring semestrPart:LPSemestrPartSecond callback:callback];
}

+ (void)getScheduleForInstituteID:(int)instituteID groupID:(int)groupID semestr:(LPSemestr)semestr semestrPart:(LPSemestrPart)part callback:(LPDataProviderCallback) callback
{
    [self LP_getDataFromURLString:[NSString stringWithFormat:@"http://schedule-lp.pp.ua/api/schedule?inst=%d&group=%d&semestr=%d&semestr_part=%d", instituteID, groupID, semestr, part] withCallback:^(NSURLResponse *response, id JSONObject, NSError *error) {
        callback(response, [self LP_scheduleArrayFrimScheduleDictionary:JSONObject], error);
    }];
}

+ (void) LP_getDataFromURLString: (NSString*) URLString withCallback: (LPDataProviderCallback) callback
{
    dispatch_queue_t currentQueue = dispatch_get_current_queue();
    dispatch_async(queue, ^{
        NSURL *URL = [NSURL URLWithString:URLString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//            id JSON = [data objectFromJSONData];
//            NSLog(@"%@", JSON);
//            callback(response, JSON, error);
//        }];
        NSURLResponse *response;
        NSError *error;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        id JSON = [data objectFromJSONData];
        NSLog(@"URL - %@", URLString);
        NSLog(@"%@", JSON);
        dispatch_async(currentQueue, ^{
            callback(response, JSON, error);
        });
    });
}

+ (NSArray*) LP_scheduleArrayFrimScheduleDictionary:(NSDictionary*)dictionary
{
    NSMutableArray *result = [NSMutableArray array];
    if([dictionary isKindOfClass:[NSDictionary class]] == NO)
    {
        NSLog(@"%@", dictionary);
        return nil;
    }
    NSArray *days = [dictionary.allKeys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSArray *days = @[@"Пн", @"Вт", @"Ср", @"Чт", @"Пт", @"Сб", @"Нд"];
        for(NSString *day in days)
        {
            if([day isEqualToString:obj1]) return NSOrderedAscending;
            if([day isEqualToString:obj2]) return NSOrderedDescending;
        }
        return NSOrderedSame;
    }];
    for(NSString *day in days)
    {
        NSMutableDictionary *daySchedule = dictionary[day];
        NSArray *lessonNumbers = [daySchedule.allKeys sortedArrayUsingSelector:@selector(compare:)];
        for(NSString * lessonNumber in lessonNumbers)
        {
            NSArray *oldLessonArray = daySchedule[lessonNumber];
            for(NSDictionary *oldLessonDict in oldLessonArray)
            {
                NSLog(@"%@", oldLessonDict);
                NSMutableDictionary *lessonDict = [NSMutableDictionary dictionaryWithDictionary:oldLessonDict];
                lessonDict[@"day"] = day;
                lessonDict[@"index"] = lessonNumber;
                [result addObject:lessonDict];
            }
        }
    }
    return result;
}

@end
