//
//  LPLesson.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/12/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LPGroup;

@interface LPLesson : NSManagedObject

@property (nonatomic, retain) NSString * auditorium;
@property (nonatomic, retain) NSString * corp;
@property (nonatomic, retain) NSString * day;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * teacher;
@property (nonatomic, retain) NSNumber * week;
@property (nonatomic, retain) NSNumber * subgroup;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) LPGroup *group;

@end
