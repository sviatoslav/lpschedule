//
//  LPDataManager.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/4/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "SYCoreDataManager.h"

@class LPGroup;

@interface LPDataManager : SYCoreDataManager

@property (nonatomic, readonly) NSString* auditoriumSeparator;
- (NSArray*) institutes;
- (NSArray*) workDays;
- (NSArray*) lessonNumbers;
- (NSArray*) workDaysForGroup: (LPGroup*) group;
- (void) updateData;
- (NSFetchedResultsController*) scheduleFetchedResultsControllerForInstitute:(NSString*) institute group:(NSString*) group day:(NSString*) day;
@property (nonatomic, readonly) NSArray * auditoriums;
- (NSDictionary*) availableAuditoriumsForDay:(NSString*) day lessonNumber:(NSString*) lessonNumber;

@end
