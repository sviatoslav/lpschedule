//
//  LPGroup.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPGroup.h"
#import "LPInstitute.h"
#import "LPLesson.h"


@implementation LPGroup

@dynamic code;
@dynamic name;
@dynamic institute;
@dynamic lessons;

@end
