//
//  LPDataManager.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/4/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "NSManagedObject+SY.h"
#import "LPDataManager.h"
#import "LPDataProvider.h"
#import "LPInstitute.h"
#import "LPGroup.h"
#import "LPLesson.h"

@interface LPDataManager()

@property (nonatomic, readwrite) NSArray *auditoriums;

@end

@implementation LPDataManager

- (void)updateData
{
    [LPDataProvider getInstitutesWithCallback:^(NSURLResponse *response, id JSONObject, NSError *error) {
       NSLog(@"Institutes retrieved.");
       for(NSDictionary *institueteDictionaryValue in JSONObject)
       {
           LPInstitute *institute = [self createInstanceOfClass:[LPInstitute class]];
           institute.dictionaryValue = institueteDictionaryValue;
           NSLog(@"Get groups for institute %@", institute);
           [LPDataProvider getGroupsForInstituteID:[institute.code intValue] withCallback:^(NSURLResponse *response, id JSONObject, NSError *error) {
               NSLog(@"Groups list for %@ retrieved.", institute.name);
               for(NSDictionary *groupDictionaryValue in JSONObject)
               {
                   LPGroup *group = [self createInstanceOfClass:[LPGroup class]];
                   group.dictionaryValue = groupDictionaryValue;
                   [institute addGroupsObject:group];
                   [LPDataProvider getScheduleForInstituteID:[institute.code intValue] groupID:group.code.intValue callback:^(NSURLResponse *response, id JSONObject, NSError *error) {
                       NSLog(@"Schedule retrieved for %@ %@", institute.name, group.name);
                       for(NSDictionary *lessonDictionaryValue in JSONObject)
                       {
                           LPLesson *lesson = [self createInstanceOfClass:[LPLesson class]];
                           lesson.dictionaryValue = lessonDictionaryValue;
                           [group addLessonsObject:lesson];
                           [self save];
                       }
                   }];
               }
           }];
       }
    }];
}

- (NSArray *)workDays
{
    return @[@"Пн", @"Вт", @"Ср", @"Чт", @"Пт"];
}

- (NSArray *) lessonNumbers
{
    return @[@"1", @"2", @"3", @"4", @"5", @"6"];
}

- (NSArray *)institutes
{
    return [self instancesOfClass:[LPInstitute class]];
}

- (NSArray*) workDaysForGroup:(LPGroup *)group
{
    NSMutableSet *days = [NSMutableSet set];
    for(LPLesson *lesson in group.lessons)
    {
        [days addObject:lesson.day];
    }
    return [days allObjects];
}

- (NSFetchedResultsController*) scheduleFetchedResultsControllerForInstitute:(NSString*) institute group:(NSString*) group day:(NSString*) day
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(group.institute.name == %@) AND (group.name == %@) AND (day == %@)", institute, group, day];
    NSSortDescriptor *sortDesctriptor = [NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES];
    return [self createFetchedResultsControllerForClass:[LPLesson class] sectionNameKeyPath:@"objectID" filteredUsingPredicate:predicate sortedUsingDescriptors:@[sortDesctriptor]];
}

- (NSString *)auditoriumSeparator
{
    return @"н.к. ";
}

- (NSArray *)auditoriums
{
    if(_auditoriums == nil)
    {
        NSMutableSet *results = [[NSMutableSet alloc] init];
        NSArray *lessons = [self instancesOfClass:[LPLesson class]];
        for(LPLesson *lesson in lessons)
        {
            if([lesson.auditorium rangeOfString:self.auditoriumSeparator].location != NSNotFound)
            {
                [results addObject:lesson.auditorium];
            }
        }
        _auditoriums = [results.allObjects sortedArrayUsingSelector:@selector(compare:)];
    }
    
    return _auditoriums;
}

- (NSArray*) LP_unavailbaleAuditoriumsForDay: (NSString*) day lessonNumber: (NSString*) lessonNumber
{
    NSMutableSet *results = [[NSMutableSet alloc] init];
    NSArray *lessons = [self instancesOfClass:[LPLesson class] filteredUsingPredicate:[NSPredicate predicateWithFormat:@"day == %@ AND index == %@", day, lessonNumber]];
    for(LPLesson *lesson in lessons)
    {
        [results addObject:lesson.auditorium];
    }
    return [results.allObjects sortedArrayUsingSelector:@selector(compare:)];
}

- (NSDictionary *)availableAuditoriumsForDay:(NSString *)day lessonNumber:(NSString *)lessonNumber
{
    NSMutableSet *all = [NSMutableSet setWithArray:self.auditoriums];
    NSSet *unavailable =[NSSet setWithArray:[self LP_unavailbaleAuditoriumsForDay:day lessonNumber:lessonNumber]];
    [all minusSet:unavailable];
    NSArray *availableAuditoriums = [all.allObjects sortedArrayUsingSelector:@selector(compare:)];
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    for(NSString *str in availableAuditoriums)
    {
        NSArray *components = [str componentsSeparatedByString:self.auditoriumSeparator];
        if(components.count == 2)
        {
            NSMutableArray *arr = [result[components[0]] mutableCopy];
            if(arr == nil) arr = [[NSMutableArray alloc] init];
            [arr addObject:components[1]];
            result[components[0]] = arr;
        }
    }
    return result;
}

@end
