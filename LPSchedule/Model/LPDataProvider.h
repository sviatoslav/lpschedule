//
//  LPDataProvider.h
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 5/2/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    LPSemestrAutumn = 0,
    LPSemestrSpring = 1
} LPSemestr;

typedef enum{
    LPSemestrPartFirst = 1,
    LPSemestrPartSecond = 2
} LPSemestrPart;

typedef void(^LPDataProviderCallback)(NSURLResponse *response, id JSONObject, NSError *error);

@interface LPDataProvider : NSObject

+ (void) getInstitutesWithCallback:(LPDataProviderCallback)callback;
+ (void) getGroupsForInstituteID: (int) instituteID withCallback: (LPDataProviderCallback) callback;
+ (void) getScheduleForInstituteID:(int)instituteID groupID:(int)groupID callback:(LPDataProviderCallback)callback;
+ (void) getScheduleForInstituteID: (int) instituteID groupID: (int) groupID semestr: (LPSemestr) semestr semestrPart: (LPSemestrPart) part callback: (LPDataProviderCallback) callback;

@end
