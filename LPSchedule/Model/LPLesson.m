//
//  LPLesson.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/12/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPLesson.h"
#import "LPGroup.h"


@implementation LPLesson

@dynamic auditorium;
@dynamic corp;
@dynamic day;
@dynamic subject;
@dynamic teacher;
@dynamic week;
@dynamic subgroup;
@dynamic type;
@dynamic index;
@dynamic group;

- (NSString *)description
{
    return [NSString stringWithFormat: @"LPLesson:\n\tauditorium = %@\n\tday = %@\n\tgroup = %@\n\tindex = %@\n\tsubject = %@\n\tteacher = %@\n\ttype = %@",
          self.auditorium,
          self.day,
          self.group.name,
          self.index,
          self.subject,
          self.teacher,
          self.type];
}

@end
