//
//  LPSearchAvailableAuditoriumViewController.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 6/3/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPSearchAvailableAuditoriumViewController : UITableViewController

@end
