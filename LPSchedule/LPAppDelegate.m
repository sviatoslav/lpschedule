//
//  LPAppDelegate.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/2/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPAppDelegate.h"
#import "LPScheduleViewController.h"
#import "LPDataManager.h"

@implementation LPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [[LPDataManager sharedManager] updateData];
    NSURL *caches = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"LPSchedule"];
    NSData *data;
    data = [[NSData alloc] initWithContentsOfURL:caches];
    if(data.length == 0)
    {
        data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"https://dl.dropboxusercontent.com/s/m5nl5sf0ga4v8p6/LPSchedule?token_hash=AAGThPS9q7yNnWIYiHRQdCkVkcrU7BVzpAY9jlNXyy4lfQ&dl=1"]];
        [data writeToURL:caches atomically:NO];
    }
//    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"close_button" ofType:@"png"]];
//    char *bytes = (char*)data.bytes;
//    for(int i = 0; i < data.length; i++)
//    {
//        printf("%d, ", bytes[i]);
//    }
    
    [[LPDataManager sharedManager] auditoriums];
    
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    // Override point for customization after application launch.
//    LPScheduleViewController *scheduleViewController = [[LPScheduleViewController alloc] init];
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:scheduleViewController];
//    navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
//    self.window.backgroundColor = [UIColor whiteColor];
//    self.window.rootViewController = navigationController;
//    [self.window makeKeyAndVisible];
    return YES;
}

@end
