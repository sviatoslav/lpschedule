//
//  LPSearchAvailableAuditoriumViewController.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 6/3/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPSearchAvailableAuditoriumViewController.h"
#import "LPScheduleFilterTableViewDelegate.h"
#import "SINavigationMenuView.h"
#import "LPDataManager.h"
#import "SYAlertView.h"

@interface LPSearchAvailableAuditoriumViewController ()<SINavigationMenuDelegate>

@property (nonatomic, strong) SINavigationMenuView *menu;
@property (nonatomic, strong) NSString *day;
@property (nonatomic, strong) NSString *lessonNumber;
@property (nonatomic, strong) LPScheduleFilterTableViewDelegate *dayDelegate;
@property (nonatomic, strong) LPScheduleFilterTableViewDelegate *lessonNumberDelegate;
@property (nonatomic, strong) NSDictionary *auditoriums;
@property (nonatomic, strong) NSArray *corps;

@end

@implementation LPSearchAvailableAuditoriumViewController

- (void)setAuditoriums:(NSDictionary *)auditoriums
{
    _auditoriums = auditoriums;
    self.corps = [auditoriums.allKeys sortedArrayUsingSelector:@selector(compare:)];
    [self.tableView reloadData];
}

-(NSString *)day
{
    return _day ? _day : [[LPDataManager sharedManager] workDays][0];
}

- (NSString *)lessonNumber
{
    return _lessonNumber ? _lessonNumber : [[LPDataManager sharedManager] lessonNumbers][0];
}

- (LPScheduleFilterTableViewDelegate *)dayDelegate
{
    if(_dayDelegate == nil)
    {
        _dayDelegate = [[LPScheduleFilterTableViewDelegate alloc] init];
        LPSearchAvailableAuditoriumViewController *controller = self;
        _dayDelegate.didSelectRowCallback = ^(int row){
            controller.day = controller.dayDelegate.items[row];
            [controller LP_refreshMenuItems];
        };
        
    }
    _dayDelegate.items = [[LPDataManager sharedManager] workDays];
    return _dayDelegate;
}

- (LPScheduleFilterTableViewDelegate *)lessonNumberDelegate
{
    if(_lessonNumberDelegate == nil)
    {
        _lessonNumberDelegate = [[LPScheduleFilterTableViewDelegate alloc] init];
        LPSearchAvailableAuditoriumViewController *controller = self;
        _lessonNumberDelegate.didSelectRowCallback = ^(int row){
            controller.lessonNumber = controller.lessonNumberDelegate.items[row];
            [controller LP_refreshMenuItems];
        };
        
    }
    _lessonNumberDelegate.items = [[LPDataManager sharedManager] lessonNumbers];
    return _lessonNumberDelegate;
}

- (SINavigationMenuView *)menu
{
    if(_menu == nil)
    {
        CGRect frame = CGRectMake(0.0, 0.0, 200.0, self.navigationController.navigationBar.bounds.size.height);
        _menu = [[SINavigationMenuView alloc] initWithFrame:frame title:@"Фільтр"];
    }
    return _menu;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.navigationItem) {
        [self.menu displayMenuInView:self.view];
        [self LP_refreshMenuItems];
        self.menu.delegate = self;
        self.navigationItem.titleView = self.menu;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.menu displayMenuInView:self.tableView];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.corps.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.corps[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.auditoriums[self.corps[section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = self.auditoriums[self.corps[indexPath.section]][indexPath.row];
    
    return cell;
}

- (void) LP_refreshMenuItems
{
    self.menu.items = @[
                    @{@"valueText":self.day, @"descriptionText" : @"День"},
                    @{@"valueText":self.lessonNumber, @"descriptionText" : @"Пара"}
                    ];
    if([self.day length] && [self.lessonNumber length])
    {
        self.auditoriums = [[LPDataManager sharedManager] availableAuditoriumsForDay:self.day lessonNumber:self.lessonNumber];
    }
}

- (void)didSelectItemAtIndex:(NSUInteger)index
{
    SYAlertView *alertView = [[SYAlertView alloc] init];
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    LPScheduleFilterTableViewDelegate *delegate = index ? self.lessonNumberDelegate : self.dayDelegate;
    tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, tableView.rowHeight * delegate.items.count);
    tableView.delegate = delegate;
    tableView.dataSource = delegate;
    alertView.contentView = tableView;
    [alertView show];
}

@end
