//
//  LPScheduleViewController.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "SYToolbarViewController.h"

@interface LPScheduleViewController : SYToolbarViewController

@property (nonatomic, strong) NSString *institute;
@property (nonatomic, strong) NSString *group;

@end
