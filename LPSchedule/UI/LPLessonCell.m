//
//  LPLessonCell.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPLessonCell.h"

@interface LPLessonCell()
{
    __weak IBOutlet UIButton *_indexButton;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UILabel *_teacherLabel;
    __weak IBOutlet UIButton *_corpButton;
    __weak IBOutlet UIButton *_auditoriumButton;
    __weak IBOutlet UIButton *_placeButton;
}

@end

@implementation LPLessonCell

- (void) setTeacher:(NSString *)teacher
{
    _teacher = teacher;
    _teacherLabel.text = _teacher;
}

- (void) setTitle:(NSString *)title
{
    _title = title;
    _titleLabel.text = title;
}

- (void) setAuditorium:(NSString *)auditorium
{
    NSArray *components = [auditorium componentsSeparatedByString:@" н.к. "];
    if(components.count == 1)
    {
        [_placeButton setTitle:components[0] forState:UIControlStateNormal];
    }
    if(components.count == 2)
    {
        [_corpButton setTitle:components[0] forState:UIControlStateNormal];
        [_auditoriumButton setTitle:components[1] forState:UIControlStateNormal];
    }
    _corpButton.hidden = components.count != 2;
    _auditoriumButton.hidden = components.count != 2;
    _placeButton.hidden = components.count != 1;
}

- (void)setLessonIndex:(int)lessonIndex
{
    _lessonIndex = lessonIndex;
    [_indexButton setTitle:[NSString stringWithFormat:@"%d", _lessonIndex] forState:UIControlStateNormal];
}

- (void) setType:(NSString *)type
{
    _type = type;
    UIColor *bgColor = [UIColor whiteColor];
    if ([_type isEqualToString:@"лекція"]) {
        bgColor = [UIColor colorWithRed:0.9 green:0.5 blue:0.5 alpha:0.7];
    }
    else if([_type isEqualToString:@"прак."])
    {
        bgColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.9 alpha:0.7];
    }
    else if([_type isEqualToString:@"лаб."])
    {
        bgColor = [UIColor colorWithRed:0.5 green:0.9 blue:0.5 alpha:0.7];
    }
    self.contentView.backgroundColor = bgColor;
}

+ (NSUInteger) heightForCellWithTitle:(NSString*) title width:(NSUInteger)width
{
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(width - 100, FLT_MAX)];
    return size.height + 48;
}

@end
