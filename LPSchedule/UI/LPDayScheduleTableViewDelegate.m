//
//  LPDayScheduleTableViewDelegate.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPDayScheduleTableViewDelegate.h"
#import "LPLesson.h"
#import "LPLessonCell.h"

@implementation LPDayScheduleTableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section
{
    if(section >= self.fetchedResultsController.sections.count) return 0;
    id  sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LPLesson *lesson = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSUInteger height = [LPLessonCell heightForCellWithTitle:lesson.subject width:tableView.frame.size.width - 20];
    return height;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    LPLessonCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    LPLesson *lesson = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.title = lesson.subject;
    cell.teacher = lesson.teacher;
    cell.auditorium = lesson.auditorium;
    cell.lessonIndex = [lesson.index intValue];
    cell.type = lesson.type;
    return cell;
}

@end