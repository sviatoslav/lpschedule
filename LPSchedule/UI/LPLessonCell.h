//
//  LPLessonCell.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPLessonCell : UITableViewCell

@property (nonatomic) int lessonIndex;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *teacher;
@property (nonatomic, strong) NSString *auditorium;
@property (nonatomic, strong) NSString *type;

+ (NSUInteger) heightForCellWithTitle:(NSString*) title width:(NSUInteger) width;

@end
