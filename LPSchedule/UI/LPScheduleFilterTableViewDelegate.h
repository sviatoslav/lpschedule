//
//  LPScheduleFilterTableViewDelegate.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^LPScheduleFilterDidSelectCallback)(int row);

@interface LPScheduleFilterTableViewDelegate : NSObject<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, copy) LPScheduleFilterDidSelectCallback didSelectRowCallback;

@end
