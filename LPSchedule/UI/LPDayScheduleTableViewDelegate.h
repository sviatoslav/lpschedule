//
//  LPDayScheduleTableViewDelegate.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LPDayScheduleTableViewDelegate : NSObject<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end