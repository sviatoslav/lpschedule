//
//  LPScheduleViewController.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "LPScheduleViewController.h"
#import "LPScheduleFilterTableViewDelegate.h"
#import "SINavigationMenuView.h"
#import "LPDataManager.h"
#import "LPInstitute.h"
#import "LPGroup.h"
#import "UIImage+SY.h"
#import "SYAlertView.h"
#import "LPDayScheduleTableViewDelegate.h"
#import "UITableViewController+SY.h"

@interface LPScheduleViewController ()<SINavigationMenuDelegate, UITableViewDataSource>
{
    SINavigationMenuView *_menu;
    NSArray *_institutes;
    LPScheduleFilterTableViewDelegate *_filterDelegate;
    NSMutableArray *_dayScheduleTableViewDelegate;
    NSArray *_workDays;
}
@end

@implementation LPScheduleViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    _dayScheduleTableViewDelegate = [NSMutableArray array];
    _filterDelegate = [[LPScheduleFilterTableViewDelegate alloc] init];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    _institutes = [[[LPDataManager sharedManager] institutes] sortedArrayUsingDescriptors:@[sortDescriptor]];
    LPInstitute *institute = _institutes.count > 0 ? _institutes[0] : nil;
    if(self.institute == nil)
    {
        self.institute = institute.name ? institute.name : @"";
    }
    if(self.group == nil)
    {
        self.group = [[institute.groups anyObject] name] ? [[institute.groups anyObject] name] : @"";
    }
    if (self.navigationItem) {
        CGRect frame = CGRectMake(0.0, 0.0, 200.0, self.navigationController.navigationBar.bounds.size.height);
        _menu = [[SINavigationMenuView alloc] initWithFrame:frame title:@"Фільтр"];
        //Set in which view we will display a menu
        [_menu displayMenuInView:self.view];
        [self LP_refreshMenuItems];
        _menu.delegate = self;
        self.navigationItem.titleView = _menu;
    }

}

- (void) LP_refreshMenuItems
{
    _menu.items = @[
                    @{@"valueText":self.institute, @"descriptionText" : @"Інститут"},
                    @{@"valueText":self.group, @"descriptionText" : @"Група"}
                    ];
    NSPredicate *instPredicate = [NSPredicate predicateWithFormat:@"name == %@", self.institute];
    NSPredicate *grPredicate = [NSPredicate predicateWithFormat:@"name == %@", self.group];
    LPInstitute *institute = [[_institutes filteredArrayUsingPredicate:instPredicate] lastObject];
    LPGroup *group = [[institute.groups filteredSetUsingPredicate:grPredicate] anyObject];
    NSArray *days = @[@"Пн", @"Вт", @"Ср", @"Чт", @"Пт", @"Сб", @"Нд"];
    _workDays = [[[LPDataManager sharedManager] workDaysForGroup:group] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        for(NSString *day in days)
        {
            if([day isEqualToString:obj1]) return NSOrderedAscending;
            if([day isEqualToString:obj2]) return NSOrderedDescending;
        }
        return [obj1 compare:obj2];
    }];
    
    [_dayScheduleTableViewDelegate removeAllObjects];
    NSMutableArray *viewControllers = [NSMutableArray array];
    for(NSString *day in _workDays)
    {
        LPDataManager *manager = [LPDataManager sharedManager];
        NSFetchedResultsController *fetchedResultsController = [manager scheduleFetchedResultsControllerForInstitute:self.institute group:self.group day:day];
        UITableViewController *vc = [[UITableViewController alloc] initWithStyle:UITableViewStyleGrouped];
        vc.title = day;
        LPDayScheduleTableViewDelegate *delegate = [[LPDayScheduleTableViewDelegate alloc] init];
        [_dayScheduleTableViewDelegate addObject:delegate];
        delegate.fetchedResultsController = fetchedResultsController;
        [vc.tableView registerNib:[UINib nibWithNibName:@"LPLessonCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        vc.tableView.delegate = delegate;
        vc.tableView.dataSource = delegate;
        vc.tableView.backgroundColor = [UIColor clearColor];
        vc.view.backgroundColor = [UIColor clearColor];
        [viewControllers addObject:vc];
    }
    self.viewControllers = viewControllers;
}

- (void)didSelectItemAtIndex:(NSUInteger)index
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *institutes = [[[LPDataManager sharedManager] institutes] sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSMutableArray *items = [NSMutableArray array];
    LPScheduleFilterDidSelectCallback filterCallback;
    SYAlertView *alertView = [[SYAlertView alloc] init];
    switch (index)
    {
        case 0:
        {
            for (LPInstitute *institute in institutes)
            {
                [items addObject:institute.name];
            }
            filterCallback = ^(int row)
            {
                [alertView dismissWithClickedButtonIndex:0 animated:YES];
                NSArray *groups = [[institutes[row] groups] sortedArrayUsingDescriptors:@[sortDescriptor]];
                if([groups count] > 0)
                {
                    self.institute = items[row];
                    self.group = [groups[0] name];
                }
                else
                {
                    [self LP_showUnavailableScheduleMessage];
                }
                [self LP_refreshMenuItems];
            };
            break;
        }
        case 1:
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", self.institute];
            LPInstitute *institute = [[institutes filteredArrayUsingPredicate:predicate] lastObject];
            for(LPGroup *group in [institute.groups sortedArrayUsingDescriptors:@[sortDescriptor]])
            {
                [items addObject:group.name];
            }
            filterCallback = ^(int row)
            {
                self.group = items[row];
                [self LP_refreshMenuItems];
                [alertView dismissWithClickedButtonIndex:0 animated:YES];
            };
            break;
        }
        default:
            break;
    }
    if(items.count > 0)
    {
        _filterDelegate.items = items;
        _filterDelegate.didSelectRowCallback = filterCallback;
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, tableView.rowHeight * items.count);
        tableView.delegate = _filterDelegate;
        tableView.dataSource = _filterDelegate;
        alertView.contentView = tableView;
        [alertView show];
    }
    else
    {
        [self LP_showUnavailableScheduleMessage];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void) LP_refreshContentViewWithLessons: (NSArray*) lessons
{
    
}

- (void) LP_showUnavailableScheduleMessage
{
    SYAlertView *alertView = [[SYAlertView alloc] init];
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = NSTextAlignmentCenter;
    NSString *text = @"Вибачте.\nНа даний момент розклад для вибраного вами інституту не доступний.";
    CGSize size = [text sizeWithFont:label.font constrainedToSize:CGSizeMake(self.view.frame.size.width / 1.5f, FLT_MAX)];
    label.frame = CGRectMake(0, 0, size.width * 1.3, size.height * 1.3);
    label.text = text;
    label.numberOfLines = 0;
    alertView.contentView = label;
    [alertView show];
}

@end
